#include "scene_conchoid.h"
#include "vec2.h"
#include <vector>

// Se manda a llamar una vez cuando inicia la aplicacion
void scene_conchoid::init()
{
	std::vector<cgmath::vec2> positions = upperCurve(0.4f);
	std::vector<cgmath::vec2> down = downerCurve(0.4f);

	// Crear un identificador para un Vertex Array Object
	// Guarda el id en vao
	glGenVertexArrays(1, &vao);
	// Quiero empezar a trabajar con el siguiente vao
	glBindVertexArray(vao);

	// Crear un identificado para un Vertex Buffer Object
	// Guardar el id en positionsVBO
	glGenBuffers(1, &positionsVBO);
	// Quiero trabajar con el buffer positionsVBO
	glBindBuffer(GL_ARRAY_BUFFER, positionsVBO);
	// Crea la memoria del buffer, especifica los datos y la manda al GPU
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * positions.size(), positions.data(), GL_DYNAMIC_DRAW);
	// Prendo el atributo 0
	glEnableVertexAttribArray(0);
	// Voy a configurar el atributo 0
	// Numero de componentes
	// Tipo de dato de cada componente
	// Normalizamos los datos?
	// Desfazamiento entre los atributos en la lista
	// Apuntador a los datos si no los hemos mandado
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	// Cuando hacemos un bind con 0 -> unbind
	// Unbind de positionsVBO
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Unbind del vao
	glBindVertexArray(0);


	glGenVertexArrays(1, &vao1);
	glBindVertexArray(vao1);
	glGenBuffers(1, &positionsVBO1);
	glBindBuffer(GL_ARRAY_BUFFER, positionsVBO1);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * down.size(), down.data(), GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	

	primitiveType = GL_LINES;
}

scene_conchoid::~scene_conchoid()
{
	glDeleteVertexArrays(1, &vao1);
	glDeleteBuffers(1, &positionsVBO1);
	glDeleteVertexArrays(1, &vao);
	glDeleteBuffers(1, &positionsVBO);

}


// Se manda a llamar cuando cambiamos a esta escena
void scene_conchoid::awake()
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glPointSize(20.0f);
}

// Se manda a llamar cuando cambiamos a otra escena
void scene_conchoid::sleep()
{
	glPointSize(1.0f);
}

// Se manda a llamar 1 vez por frame
void scene_conchoid::mainLoop()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Bind del vao que tiene todos los atributos
	glBindVertexArray(vao);
	// Llamada a dibujar
	// Tipo de primitiva
	// Desde que vertice se empieza a dibujar
	// Cuantos vertices se dibujan
	glDrawArrays(primitiveType, 0, 20);
	// Unbind del vao y todos los atributos
	glBindVertexArray(0);

	glBindVertexArray(vao1);
	glDrawArrays(primitiveType, 0, 20);
	glBindVertexArray(0);



}

// Se manda a llamar cuando alguien presiona una tecla "normal".
// Una tecla normal es aquella que puede ser representada por un caracter,
// como letras o numeros.
void scene_conchoid::normalKeysDown(unsigned char key)
{
	if (key == '1') primitiveType = GL_POINTS;
	if (key == '2') primitiveType = GL_LINES;
	if (key == '3') primitiveType = GL_LINE_STRIP;
	if (key == '4') primitiveType = GL_LINE_LOOP;
	if (key == '5') primitiveType = GL_TRIANGLES;
	if (key == '6') primitiveType = GL_TRIANGLE_STRIP;
	if (key == '7') primitiveType = GL_TRIANGLE_FAN;
}

std::vector<cgmath::vec2> scene_conchoid::upperCurve(float k)
{
	std::vector<cgmath::vec2> up_Vector;
	float xMove = 0.9f / (float)10;
	float xStart = -0.9f;

	float magn = cgmath::vec2::magnitude(cgmath::vec2(-1.3f, 0.7f) - cgmath::vec2(-0.9f, k));
	for (int i = 0; i < 20; i++)
	{
		cgmath::vec2 linePoint = cgmath::vec2((xStart + xMove * i), k);
		cgmath::vec2 endPoint = cgmath::vec2::normalize(linePoint) * magn + linePoint;
		up_Vector.push_back(endPoint);
	}

	return up_Vector;
}


std::vector<cgmath::vec2> scene_conchoid::downerCurve(float k)
{
	std::vector<cgmath::vec2> up_Vector;
	float xMove = 0.9f / (float)10;
	float xStart = -0.9f;

	float magn = cgmath::vec2::magnitude(cgmath::vec2(-1.3f, 0.7f) - cgmath::vec2(-0.9f, k));
	for (int i = 0; i < 20; i++)
	{
		cgmath::vec2 linePoint = cgmath::vec2((xStart + xMove * i), k);
		cgmath::vec2 endPoint = linePoint - cgmath::vec2::normalize(linePoint) * magn ;
		up_Vector.push_back(endPoint);
	}

	return up_Vector;
}
