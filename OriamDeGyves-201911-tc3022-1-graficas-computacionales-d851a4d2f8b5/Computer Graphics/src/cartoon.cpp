#include "cartoon.h"
#include "vec2.h"
#include <vector>


cartoon::~cartoon()
{
	glDeleteVertexArrays(1, &vao1);
	glDeleteBuffers(1, &positionsVBO1);
	glDeleteVertexArrays(1, &vao2);
	glDeleteBuffers(1, &positionsVBO2);
	glDeleteVertexArrays(1, &vao3);
	glDeleteBuffers(1, &positionsVBO3);
	glDeleteVertexArrays(1, &vao4);
	glDeleteBuffers(1, &positionsVBO4);
	glDeleteVertexArrays(1, &vao5);
	glDeleteBuffers(1, &positionsVBO5);
	glDeleteVertexArrays(1, &vao6);
	glDeleteBuffers(1, &positionsVBO6);
	glDeleteVertexArrays(1, &vao7);
	glDeleteBuffers(1, &positionsVBO7);
	glDeleteVertexArrays(1, &vao8);
	glDeleteBuffers(1, &positionsVBO8);

}

// Se manda a llamar una vez cuando inicia la aplicacion
void cartoon::init()
{
	drawContinous = true;
	drawPointy = true;
	std::vector<cgmath::vec2> positions;
	positions.push_back(cgmath::vec2(-1.5f, 9.5f));
	positions.push_back(cgmath::vec2(-5.5f, 7.5f));
	positions.push_back(cgmath::vec2(-8.2f, 4.0f));
	positions.push_back(cgmath::vec2(-8.2f, 0.6f));
	positions.push_back(cgmath::vec2(-7.4f, -1.1f));
	positions.push_back(cgmath::vec2(-4.0f, -3.4f));
	positions.push_back(cgmath::vec2(-4.0f, 0.0f));
	positions.push_back(cgmath::vec2(-2.0f, 1.0f));
	positions.push_back(cgmath::vec2(2.0f, 1.0f));
	positions.push_back(cgmath::vec2(4.1f, 0.4f));
	positions.push_back(cgmath::vec2(4.0f, -3.4f));
	positions.push_back(cgmath::vec2(7.4f, -1.1f));
	positions.push_back(cgmath::vec2(9.0f, 2.0f));
	positions.push_back(cgmath::vec2(8.6f, 5.4f));
	positions.push_back(cgmath::vec2(7.0f, 8.0f));
	positions.push_back(cgmath::vec2(3.4f, 9.4f));

	for (int i = 0; i < positions.size(); i++)
	{
		positions[i] /= 9.5f;
	}
	
	std::vector<cgmath::vec2> positions0 = refine(positions);
	std::vector<cgmath::vec2> positions1 = refine(positions0);
	std::vector<cgmath::vec2> positions2 = refine(positions1);


	std::vector<cgmath::vec2> dot;
	dot.push_back(cgmath::vec2(1.8f, 7.8f));
	dot.push_back(cgmath::vec2(-1.8f, 7.8f));
	dot.push_back(cgmath::vec2(-3.0f, 5.3f));
	dot.push_back(cgmath::vec2(-1.9f, 2.8f));
	dot.push_back(cgmath::vec2(1.9f, 2.8f));
	dot.push_back(cgmath::vec2(3.0f, 5.3f));

	for (int i = 0; i < dot.size(); i++)
	{
		dot[i] /= 9.5f;
	}

	std::vector<cgmath::vec2> dot0 = refine(dot);
	std::vector<cgmath::vec2> dot1 = refine(dot0);
	std::vector<cgmath::vec2> dot2 = refine(dot1);


	std::vector<cgmath::vec2> left;
	left.push_back(cgmath::vec2(-6.9f, 6.1f));
	left.push_back(cgmath::vec2(-8.4f, 2.6f));
	left.push_back(cgmath::vec2(-7.7f, 2.6f));
	left.push_back(cgmath::vec2(-6.1f, 5.1f));
	for (int i = 0; i < left.size(); i++)
	{
		left[i] /= 9.5f;
	}

	std::vector<cgmath::vec2> left0 = refine(left);
	std::vector<cgmath::vec2> left1 = refine(left0);
	std::vector<cgmath::vec2> left2 = refine(left1);


	std::vector<cgmath::vec2> right;
	right.push_back(cgmath::vec2(8.5f, 5.8f));
	right.push_back(cgmath::vec2(7.8f, 5.6f));
	right.push_back(cgmath::vec2(8.1f, 1.9f));
	right.push_back(cgmath::vec2(9.0f, 2.0f));
	for (int i = 0; i < right.size(); i++)
	{
		right[i] /= 9.5f;
	}

	std::vector<cgmath::vec2> right0 = refine(right);
	std::vector<cgmath::vec2> right1 = refine(right0);
	std::vector<cgmath::vec2> right2 = refine(right1);


	std::vector<cgmath::vec2> eyeL;
	eyeL.push_back(cgmath::vec2(-2.1f, -0.4f));
	eyeL.push_back(cgmath::vec2(-2.1f, -2.7f));
	eyeL.push_back(cgmath::vec2(-0.9f, -2.7f));
	eyeL.push_back(cgmath::vec2(-0.9f, -0.4f));
	for (int i = 0; i < eyeL.size(); i++)
	{
		eyeL[i] /= 9.5f;
	}

	std::vector<cgmath::vec2> eyeL0 = refine(eyeL);
	std::vector<cgmath::vec2> eyeL1 = refine(eyeL0);
	std::vector<cgmath::vec2> eyeL2 = refine(eyeL1);


	std::vector<cgmath::vec2> eyeD;
	eyeD.push_back(cgmath::vec2(2.1f, -0.4f));
	eyeD.push_back(cgmath::vec2(2.1f, -2.7f));
	eyeD.push_back(cgmath::vec2(0.9f, -2.7f));
	eyeD.push_back(cgmath::vec2(0.9f, -0.4f));
	for (int i = 0; i < eyeD.size(); i++)
	{
		eyeD[i] /= 9.5f;
	}

	std::vector<cgmath::vec2> eyeD0 = refine(eyeD);
	std::vector<cgmath::vec2> eyeD1 = refine(eyeD0);
	std::vector<cgmath::vec2> eyeD2 = refine(eyeD1);


	std::vector<cgmath::vec2> smile;
	smile.push_back(cgmath::vec2(-2.7f, -3.8f));
	smile.push_back(cgmath::vec2(-2.3f, -4.0f));
	smile.push_back(cgmath::vec2(-0.8f, -5.3f));
	smile.push_back(cgmath::vec2(0.8f, -5.3f));
	smile.push_back(cgmath::vec2(2.3f, -4.0f));
	smile.push_back(cgmath::vec2(2.7f, -3.8f));
	smile.push_back(cgmath::vec2(0.0f, -4.7f));
	for (int i = 0; i < smile.size(); i++)
	{
		smile[i] /= 9.5f;
	}

	std::vector<cgmath::vec2> smile0 = refine(smile);
	std::vector<cgmath::vec2> smile1 = refine(smile0);
	std::vector<cgmath::vec2> smile2 = refine(smile1);


	std::vector<cgmath::vec2> face;
	face.push_back(cgmath::vec2(-4.0f, -3.4f));
	face.push_back(cgmath::vec2(-4.0f, 0.0f));
	face.push_back(cgmath::vec2(-2.0f, 1.0f));
	face.push_back(cgmath::vec2(2.0f, 1.0f));
	face.push_back(cgmath::vec2(4.1f, 0.4f));
	face.push_back(cgmath::vec2(4.0f, -3.4f));;
	face.push_back(cgmath::vec2(2.8f, -5.7f));
	face.push_back(cgmath::vec2(0.0f, -7.0f));
	face.push_back(cgmath::vec2(-2.8f, -5.7f));
	for (int i = 0; i < face.size(); i++)
	{
		face[i] /= 9.5f;
	}

	std::vector<cgmath::vec2> face0 = refine(face);
	std::vector<cgmath::vec2> face1 = refine(face0);
	std::vector<cgmath::vec2> face2 = refine(face1);



	// Crear un identificador para un Vertex Array Object
	// Guarda el id en vao
	glGenVertexArrays(1, &vao1);
	
	// Quiero empezar a trabajar con el siguiente vao
	glBindVertexArray(vao1);

	// Crear un identificado para un Vertex Buffer Object
	// Guardar el id en positionsVBO
	glGenBuffers(1, &positionsVBO1);
	
	// Quiero trabajar con el buffer positionsVBO
	glBindBuffer(GL_ARRAY_BUFFER, positionsVBO1);
	
	// Crea la memoria del buffer, especifica los datos y la manda al GPU
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * positions2.size(), positions2.data(), GL_DYNAMIC_DRAW);
	// Prendo el atributo 0
	glEnableVertexAttribArray(0);
	// Voy a configurar el atributo 0
	// Numero de componentes
	// Tipo de dato de cada componente
	// Normalizamos los datos?
	// Desfazamiento entre los atributos en la lista
	// Apuntador a los datos si no los hemos mandado
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	// Cuando hacemos un bind con 0 -> unbind
	// Unbind de positionsVBO
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Unbind del vao
	glBindVertexArray(0);
	/*glDeleteVertexArrays(1, &vao1);
	glDeleteBuffers(1, &positionsVBO1);*/

	glGenVertexArrays(1, &vao2);
	glBindVertexArray(vao2);
	glGenBuffers(1, &positionsVBO2);
	glBindBuffer(GL_ARRAY_BUFFER, positionsVBO2);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * dot2.size(), dot2.data(), GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	glGenVertexArrays(1, &vao3);
	glBindVertexArray(vao3);
	glGenBuffers(1, &positionsVBO3);
	glBindBuffer(GL_ARRAY_BUFFER, positionsVBO3);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * left2.size(), left2.data(), GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	glGenVertexArrays(1, &vao4);
	glBindVertexArray(vao4);
	glGenBuffers(1, &positionsVBO4);
	glBindBuffer(GL_ARRAY_BUFFER, positionsVBO4);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * right2.size(), right2.data(), GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	glGenVertexArrays(1, &vao5);
	glBindVertexArray(vao5);
	glGenBuffers(1, &positionsVBO5);
	glBindBuffer(GL_ARRAY_BUFFER, positionsVBO5);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * eyeL2.size(), eyeL2.data(), GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	glGenVertexArrays(1, &vao6);
	glBindVertexArray(vao6);
	glGenBuffers(1, &positionsVBO6);
	glBindBuffer(GL_ARRAY_BUFFER, positionsVBO6);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * eyeD2.size(), eyeD2.data(), GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	glGenVertexArrays(1, &vao7);
	glBindVertexArray(vao7);
	glGenBuffers(1, &positionsVBO7);
	glBindBuffer(GL_ARRAY_BUFFER, positionsVBO7);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * smile2.size(), smile2.data(), GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	glGenVertexArrays(1, &vao8);
	glBindVertexArray(vao8);
	glGenBuffers(1, &positionsVBO8);
	glBindBuffer(GL_ARRAY_BUFFER, positionsVBO8);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * face2.size(), face2.data(), GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);


	primitiveType1 = GL_LINE_LOOP;
	primitiveType2 = GL_LINES;
}

std::vector<cgmath::vec2> cartoon::refine(std::vector<cgmath::vec2> vv)
{
	std::vector<cgmath::vec2> refine_Vector;


	for (int i = 0; i < (vv.size() -1); i++)
	{
		cgmath::vec2 q1 = vv[i] * 0.75f + vv[i + 1] * 0.25f;
		cgmath::vec2 r1 = vv[i] * 0.25f + vv[i + 1] * 0.75f;
		refine_Vector.push_back(q1);
		refine_Vector.push_back(r1);
	}
	cgmath::vec2 q1 = vv[vv.size() - 1] * 0.75f + vv[0] * 0.25f;
	cgmath::vec2 r1 = vv[vv.size() - 1] * 0.25f + vv[0] * 0.75f;
	refine_Vector.push_back(q1);
	refine_Vector.push_back(r1);
	return refine_Vector;
}

// Se manda a llamar cuando cambiamos a esta escena
void cartoon::awake()
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glPointSize(20.0f);
}

// Se manda a llamar cuando cambiamos a otra escena
void cartoon::sleep()
{
	glPointSize(1.0f);
}

// Se manda a llamar 1 vez por frame
void cartoon::mainLoop()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if (drawContinous)
	{
	// Bind del vao que tiene todos los atributos
		glBindVertexArray(vao1);
	
		// Llamada a dibujar
		// Tipo de primitiva
		// Desde que vertice se empieza a dibujar
		// Cuantos vertices se dibujan
		glDrawArrays(primitiveType1, 0, 128);
		// Unbind del vao y todos los atributos
		glBindVertexArray(0);

		glBindVertexArray(vao2);
		glDrawArrays(primitiveType1, 0, 48);
		glBindVertexArray(0);

		glBindVertexArray(vao3);
		glDrawArrays(primitiveType1, 0, 32);
		glBindVertexArray(0);

		glBindVertexArray(vao4);
		glDrawArrays(primitiveType1, 0, 32);
		glBindVertexArray(0);
	
		glBindVertexArray(vao5);
		glDrawArrays(primitiveType1, 0, 32);
		glBindVertexArray(0);
	
		glBindVertexArray(vao6);
		glDrawArrays(primitiveType1, 0, 32);
		glBindVertexArray(0);
	
		glBindVertexArray(vao7);
		glDrawArrays(primitiveType1, 0, 56);
		glBindVertexArray(0);
	
		glBindVertexArray(vao8);
		glDrawArrays(primitiveType1, 0, 72);
		glBindVertexArray(0);
	}

	if (drawPointy)
	{
		glBindVertexArray(vao1);

		// Llamada a dibujar
		// Tipo de primitiva
		// Desde que vertice se empieza a dibujar
		// Cuantos vertices se dibujan
		glDrawArrays(primitiveType2, 0, 128);
		// Unbind del vao y todos los atributos
		glBindVertexArray(0);

		glBindVertexArray(vao2);
		glDrawArrays(primitiveType2, 0, 48);
		glBindVertexArray(0);

		glBindVertexArray(vao3);
		glDrawArrays(primitiveType2, 0, 32);
		glBindVertexArray(0);

		glBindVertexArray(vao4);
		glDrawArrays(primitiveType2, 0, 32);
		glBindVertexArray(0);

		glBindVertexArray(vao5);
		glDrawArrays(primitiveType2, 0, 32);
		glBindVertexArray(0);

		glBindVertexArray(vao6);
		glDrawArrays(primitiveType2, 0, 32);
		glBindVertexArray(0);

		glBindVertexArray(vao7);
		glDrawArrays(primitiveType2, 0, 56);
		glBindVertexArray(0);

		glBindVertexArray(vao8);
		glDrawArrays(primitiveType2, 0, 72);
		glBindVertexArray(0);
	}
	

}

// Se manda a llamar cuando alguien presiona una tecla "normal".
// Una tecla normal es aquella que puede ser representada por un caracter,
// como letras o numeros.
void cartoon::normalKeysDown(unsigned char key)
{
	if (key == '1') { 
		primitiveType1 = GL_POINTS;
		primitiveType2 = GL_POINTS;
	}
	if (key == '2') { 
		primitiveType1 = GL_LINES; 
		primitiveType2 = GL_LINES; 
	}
	if (key == '3') { 
		primitiveType1 = GL_LINE_STRIP; 
		primitiveType2 = GL_LINE_STRIP; 
	}
	if (key == '4') {
		primitiveType1 = GL_LINE_LOOP; 
		primitiveType2 = GL_LINE_LOOP; 
	}
	if (key == '5') {
		primitiveType1 = GL_TRIANGLES; 
		primitiveType2 = GL_TRIANGLES; 
	}
	if (key == '6') {
		//primitiveType1 = GL_TRIANGLE_STRIP;
		if (drawPointy)
		{
			drawPointy = false;
		}
		else
		{
			drawPointy = true;
		}
	}
	if (key == '7') {
		//primitiveType1 = GL_TRIANGLE_FAN; 
		if (drawContinous)
		{
			drawContinous = false;
		}
		else
		{
			drawContinous = true;
		} 
	}
}


