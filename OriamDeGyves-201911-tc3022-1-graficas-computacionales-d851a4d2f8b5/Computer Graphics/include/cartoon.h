#pragma once 

#include "scene.h"
#include <vector>
#include "vec2.h"

class cartoon : public scene
{
public:
	bool drawPointy;
	bool drawContinous;
	~cartoon();
	void init();
	void awake();
	void sleep();
	void reset() { }
	void mainLoop();
	void resize(int width, int height) { }
	void normalKeysDown(unsigned char key);
	void normalKeysUp(unsigned char key) { }
	void specialKeys(int key) { }
	void passiveMotion(int x, int y) { }
	std::vector<cgmath::vec2> refine(std::vector<cgmath::vec2> vv);

private:
	// Este es el manager de atributos
	GLuint vao1;
	// Este es el buffer con el atributo de posicion
	GLuint positionsVBO1;
	GLenum primitiveType1;

	GLuint vao2;
	GLuint positionsVBO2;
	GLenum primitiveType2;

	GLuint vao3;
	GLuint positionsVBO3;
	
	GLuint vao4;
	GLuint positionsVBO4;

	GLuint vao5;
	GLuint positionsVBO5;

	GLuint vao6;
	GLuint positionsVBO6;

	GLuint vao7;
	GLuint positionsVBO7;

	GLuint vao8;
	GLuint positionsVBO8;
	
};
