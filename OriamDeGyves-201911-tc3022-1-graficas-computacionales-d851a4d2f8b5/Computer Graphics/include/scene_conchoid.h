#pragma once
#include "scene.h"
#include <vector>
#include "vec2.h"

class scene_conchoid : public scene
{
public:
	void init();
	~scene_conchoid();
	void awake();
	void sleep();
	void reset() { }
	void mainLoop();
	void resize(int width, int height) { }
	void normalKeysDown(unsigned char key);
	void normalKeysUp(unsigned char key) { }
	void specialKeys(int key) { }
	void passiveMotion(int x, int y) { }
	std::vector<cgmath::vec2> upperCurve(float k);
	std::vector<cgmath::vec2> downerCurve(float k);

private:
	// Este es el manager de atributos
	GLuint vao;
	// Este es el buffer con el atributo de posicion
	GLuint positionsVBO;

	GLuint vao1;
	GLuint positionsVBO1;


	GLenum primitiveType;
};