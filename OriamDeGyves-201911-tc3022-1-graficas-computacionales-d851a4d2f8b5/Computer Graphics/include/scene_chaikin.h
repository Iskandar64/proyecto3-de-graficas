#pragma once 

#include "scene.h"
#include <vector>
#include "vec2.h"

class scene_chaikin : public scene
{
public:
	bool drawPointy;
	bool drawContinous;
	~scene_chaikin();
	void init();
	void awake();
	void sleep();
	void reset() { }
	void mainLoop();
	void resize(int width, int height) { }
	void normalKeysDown(unsigned char key);
	void normalKeysUp(unsigned char key) { }
	void specialKeys(int key) { }
	void passiveMotion(int x, int y) { }
	std::vector<cgmath::vec2> refine(std::vector<cgmath::vec2> vv);

private:
	// Este es el manager de atributos
	GLuint vao1;
	// Este es el buffer con el atributo de posicion
	GLuint positionsVBO1;
	GLenum primitiveType1;

	GLuint vao2;
	GLuint positionsVBO2;
	GLenum primitiveType2;

	GLuint vao3;
	GLuint positionsVBO3;

	GLuint vao4;
	GLuint positionsVBO4;

	GLuint vao5;
	GLuint positionsVBO5;

	GLuint vao6;
	GLuint positionsVBO6;

	GLuint vao7;
	GLuint positionsVBO7;

	GLuint vao8;
	GLuint positionsVBO8;


	GLuint vao9;
	GLuint positionsVBO9;

	GLuint vao10;
	GLuint positionsVBO10;

	GLuint vao11;
	GLuint positionsVBO11;

	GLuint vao12;
	GLuint positionsVBO12;

	GLuint vao13;
	GLuint positionsVBO13;

	GLuint vao14;
	GLuint positionsVBO14;

	GLuint vao15;
	GLuint positionsVBO15;

	GLuint vao16;
	GLuint positionsVBO16;

};

